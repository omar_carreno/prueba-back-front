/*global angular:true*/
/*global firebase:true*/

const todoSrv = ($q, $http, $firebaseAuth) => {
    const responseTodoSrv = {
        createTodo: (response) => {
            return $q((resolve, reject) => {
                $http.post("http://localhost:9090/importExcel/imporExcel", response)
                    .then((res) => {
                        resolve(res.data);
                    })
                    .catch((error) => {
                        reject(error.data);
                    });
            });
        },
        authUserWithMail: function (email, password) {
            return $q((resolve, reject) => {
                console.log('🤬authUserWithMail');
                $firebaseAuth().$signInWithEmailAndPassword(email, password)
                    .then((authData) => {
                        console.log('🤬authData', authData);
                        resolve(authData);
                    }).catch((error) => {
                        console.log('🤬error', error);
                        reject(error);
                    });
            });
        },
    };
    return responseTodoSrv;
};
todoSrv.$inject = ["$q", "$http", "$firebaseAuth"];
angular.module("phonecatApp").factory("todoSrv", todoSrv);