/*global angular:true*/

const runHome = function () {
	"use strict";
};
runHome.$inject = [];
angular
	.module("phonecatApp", ["firebase"])
	.run(runHome);

firebase.initializeApp(firebaseConfig);

var runHomeConfig = function ($httpProvider) {
	$httpProvider.interceptors.push(["$q", ($q) => ({
		request: (httpConfig) => {
			httpConfig.headers["Content-Type"] = "application/json";
			return httpConfig;
		},
		responseError: (response) => $q.reject(response)
	})]);
	$httpProvider.interceptors.push(["$q", "tokenSrv", ($q, tokenSrv) => {
		var response = {
			request: (config) => {
				var deferred = $q.defer();
				tokenSrv.getToken()
					.then((token) => {
						if (config.url.substring(config.url.length - 4) != ".php") {
							config.headers.authorization = token;
						}
						deferred.resolve(config);
					}).catch(() => {
						deferred.resolve(config);
					});
				return deferred.promise;
			},
			responseError: (responses) => $q.reject(responses)
		};
		return response;
	}]);
};
runHomeConfig.$inject = ["$httpProvider"];
angular.module("phonecatApp").config(runHomeConfig);